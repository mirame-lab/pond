<?php

namespace App\Http\Controllers;

use App\Models\TierItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect,Response;
class TierItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return query of tier items.
        // $items = DB::table('tieritems')->get();
        // return view();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TierItems  $tierItems
     * @return \Illuminate\Http\Response
     */
    public function show(TierItems $tierItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param   int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $where = array('id' => $id);
        $tierItems = DB::table('tieritems')->where($where)->first();
        return Response::json($tierItems); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TierItems  $tierItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TierItems $tierItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TierItems  $tierItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(TierItems $tierItems)
    {
        //
    }
}
