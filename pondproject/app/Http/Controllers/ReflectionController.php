<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Reflection;
use Illuminate\Http\Request;
use Redirect,Response;

class ReflectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $refl = DB::select('select * from reflections');

        $refl = [
            'item' => $request->item,
            'owner' => $request->owner,
            'content' => $request->reflection
        ];

        Reflection::create($refl);
      
        return redirect('/tasks#tasks')->with('success', 'Task saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reflection  $reflection
     * @return \Illuminate\Http\Response
     */
    public function show(Reflection $reflection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reflection  $reflection
     * @return \Illuminate\Http\Response
     */
    public function edit(Reflection $reflection)
    {       
        
        return Response::json($reflection); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reflection  $reflection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reflection $reflection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reflection  $reflection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reflection $reflection)
    {
        //
    }
}
