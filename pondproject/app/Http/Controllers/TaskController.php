<?php

namespace App\Http\Controllers;
use App\Models\Task; //import task
use App\Models\TierItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Redirect,Response;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
  
        //get list of db
        $events = [];
        $task = DB::table('tasks')->where('owner',auth()->user()->email)->get();
        $refl = DB::table('reflections')->where('owner',auth()->user()->email)->get();
        $taskcompnum = DB::table('tasks')->where(
            [
                ['owner','=', auth()->user()->email],
                ['isCompleted','=','1']
            ]
        )->count();
        $items = DB::table('tieritems')->get();
        $currPoints= $this->getPoints();
        $hof = $this->getLeaderboard();
        $currTier = $this->getTier();
        $lvl = $this->getLevel();

        foreach($task as $t):
            array_push($events,[
                "title"=>$t->title,
                "start"=>$t->startdate,
                "end"=>$t->deadline
            ]);
        endforeach;
        $encoded = json_encode($events);
        //send to index view
        return view('index',compact('task','currPoints','hof','currTier','taskcompnum','lvl','items','refl','encoded'));
        // return $encoded;
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $task = DB::select('select * from tasks');

        $task = [
            'title' => $request->title,
            'detail' => $request->detail,
            'deadline' => $request->deadline,
            'owner'=> $request->owner,
            'startdate'=>$request->start
        ];

        //return redirect('/index')->with('success', 'Task updated!');
        $taskid=$request->id;
        Task::updateOrCreate(
            ['id' => $taskid],
            $task);
        // $task->save();
        return redirect('/tasks#tasks')->with('success', 'Task saved!');
        //return $jsontask;
        // return redirect()->back();
    }



 public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $where = array('id' => $id);
        $task = Task::where($where)->first();
        return Response::json($task); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        Task::whereId($id)->update([
            'isCompleted'=> 1
        ]);
        return redirect('/tasks#tasks')->with('success', 'Task saved!');
        // return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect('/tasks#tasks')->with('success', 'Task deleted!');
    }


    public function getPoints(){
        $url = 'api.tenenet.net/getPlayer?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $user = auth()->user();
        $player = Http::post($url.'&alias='.$user->name);
        $player_score = $player['message']['score'][0]['value'];

        return $player_score;
    }

    public function getTier(){
        $url = 'api.tenenet.net/getPlayer?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $user = auth()->user();
        $player = Http::post($url.'&alias='.$user->name);
        $player_tier = $player['message']['score'][2]['value'];

        return $player_tier;
    }

    public static function addPoints(){
        $url = 'api.tenenet.net/insertPlayerActivity?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $user = auth()->user();
        $player = Http::post($url.'&alias='.$user->name.'&id=mainpoints'.'&operator=add'.'&value=100'); //add 100 to player's mainpoints
    }
    public static function addLvlPoints($prog){
        $url = 'api.tenenet.net/getPlayer?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        
        $user = auth()->user();
        $player = Http::post($url.'&alias='.$user->name.'&id=completed_points'.'&operator=add'.'&value=1'); //add 1 to player's completed points
        
    }
    public static function delPoints(){
        $url = 'api.tenenet.net/insertPlayerActivity?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $user = auth()->user();
        $player = Http::post($url.'&alias='.$user->name.'&id=mainpoints'.'&operator=remove'.'&value=1000'); //add 100 to player's mainpoints
    }
    public static function getLevel(){
        $url = 'api.tenenet.net/getPlayer?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $user = auth()->user();
        $player = Http::post($url.'&alias='.$user->name);
        $player_lvl = $player['message']['score'][1]['value'];

        return $player_lvl;
    }


    public function getLeaderboard() {
        $url2 = 'api.tenenet.net/getPlayer?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $url = 'api.tenenet.net/getLeaderboard?token=1b1a8f39fff90c39145296e00f8b344c'; //base url
        $response = Http::post($url.'&id=hof_alt');
        
        $hof_list_full = $response['message']['data'];
        $shortlisted_len = sizeof($response['message']['data']) <10 ? sizeof($response['message']['data']): 10;
        $hof_list = array();
        $hof_player_scores = array();

        //Get only top 10
        for($i=0; $i<$shortlisted_len; $i++){
            $response2 = Http::post($url2.'&alias='.$hof_list_full[$i]['alias']);
            array_push($hof_list,$hof_list_full[$i]);
            array_push($hof_player_scores, $response2['message']['score'][0]['value']);

        }

        return ['names'=> $hof_list, 'scores'=> $hof_player_scores];
    }


}
