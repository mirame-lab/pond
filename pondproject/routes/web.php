<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::group(['prefix' => 'admin'], function () {
//     Voyager::routes();
// });

// Route::get('/tieritems',[App\Http\Controllers\TierItemsController::class,'all']);
// Route::get('/reflection',[App\Http\Controllers\ReflectionController::class,'all']);
Route::resource('tasks', TaskController::class);
Route::resource('tieritems',App\Http\Controllers\TierItemsController::class);
Route::resource('reflection',App\Http\Controllers\ReflectionController::class);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/index', function () {
    return view('index');
});
