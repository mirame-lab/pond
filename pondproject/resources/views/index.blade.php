@extends('mainlayout')

 @section('header')
    @include('header')
 @endsection
 
 @section('content')

    @include('old_content.facts')
    @include('old_content.about')
    @include('old_content.resume')
    @include('old_content.portfolio')
    @include('old_content.services')
    @include('old_content.contact')
    @include('old_content.skills')
    @include('old_content.testimonials')

@endsection