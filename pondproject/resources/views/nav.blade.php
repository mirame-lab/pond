<nav class="nav-menu">
  <ul>
    <li class="active"><a href="#hero"><i class="bx bx-home"></i> <span>Home</span></a></li>
    <li><a href="#pond"><i class="bx bx-joystick"></i> <span>My Pond</span></a></li> <!--Pond-->
    <li><a href="#tasks"><i class="bx bx-book-content"></i> <span>Tasks</span></a></li> <!--About-->
    <li><a href="#calendar"><i class="bx bx-calendar"></i> <span>Calendar</span></a></li> <!--Resume-->
    <li><a href="#hof"><i class="bx bx-coin"></i>Pond Shop</a></li> <!--Portfolio-->
    <li><a href="#shop"><i class="bx bx-crown"></i>Hall of Fame</a></li> <!--Services-->
    <li><a href="#settings"><i class="bx bx-badge"></i>Achievements</a></li> <!--Badges-->
    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <i class="bx bx-user-circle"></i>  {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
    </li>

  </ul>
</nav><!-- .nav-menu -->
