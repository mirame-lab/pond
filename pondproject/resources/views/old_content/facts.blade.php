<!-- ======= Facts Section ======= -->

<section id="pond" class="facts">
  <div class="container">

    <div class="section-title">
      <h2>MY POND</h2>
      <p>Preview your decorated Pond here and expand your creativity! p/s: draw something on canvas.</p>
    </div>
    <button type="button" class="collapsible">Items</button>
        <div class="content">

          <?php foreach($refl as $claimeditems):?>
            <div class="drag-drop" id="select" data-tooltip="<?php echo $claimeditems->content;?>"data-tooltip-location="right" data-id="{{$claimeditems->id}}"> 
              <img src=<?php echo $claimeditems->item; ?>>
            </div>         
          <?php endforeach; ?>
        </div>

      <p id="add"> 



    <div id="outer-dropzone" class="dropzone">
 
      <canvas class="rainbow-pixel-canvas"></canvas>

      </div>
    </div>

</section><!-- End Facts Section -->

<script>
  var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}

$(document).ready(function () {
/* Edit task */
$('body').on('click', '#select', function () {
  var itemid = $(this).data('id');
    $.get('reflection/'+itemid+'/edit', function (data) {
      $('#add').after('<div id="yes-drop" class="drag-drop" data-tooltip="'+data.content+'"> <img src="'+data.item+'" id="draggable"></div>');
      })
  });
});

</script> 