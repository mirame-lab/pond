<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container" data-aos="fade-in">
      <h1>Welcome to Pond</h1>
      <p>Pond is&nbsp<span class="typed" data-typed-items=" fun, easy, your #1 tasks manager"></span></p>
      @guest
            @if (Route::has('login'))
                <button type='button' class="">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </button>
            @endif
                            
            @if (Route::has('register'))
                 <button type='button' class="">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                 </button>
            @endif
           @else
           <p>Hello&nbsp{{ Auth::user()->name }}&#10024;</p> <br>
           <p>You are rank &nbsp{{TaskController::getLevel()}}</p>
           <button type='button' class="">
           <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
          </a>
          </button>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
          </form>
          <button type='button'><a href="/tasks">Go to Home</a></button>
      @endguest
    </div>
  </section>
  <!-- End Hero -->
