<!-- ======= HOF Section ======= -->
<section id="hof" class="portfolio section-bg">
  <div class="container">

    <div class="section-title">
      <h2>POND SHOP</h2>
      <p> Complete every tasks and increase your points to unlock each Tier for upgraded icons! </p>
    </div>

    <div class="row" data-aos="fade-up">
      <div class="col-lg-12 d-flex justify-content-center">
        <ul id="portfolio-flters">
          
          <?php $unlocked = array();?>
          @if($currPoints >= 100)
          <?php array_push($unlocked,"filter-t1");?>
          <li data-filter=".filter-t1">Tier 1</li>
            @if($currPoints >= 200)
            <?php array_push($unlocked,"filter-t2");?>
            <li data-filter=".filter-t2">Tier 2</li>
              @if($currPoints >= 300)
              <?php array_push($unlocked,"filter-t3");?>
              <li data-filter=".filter-t3">Tier 3</li>
                @if($currPoints >= 400)
                <?php array_push($unlocked,"filter-t4");?>
                <li data-filter=".filter-t4">Tier 4</li>
                  @if($currPoints >= 500)
                  <?php array_push($unlocked,"filter-t5");?>
                  <li data-filter=".filter-t5">Tier 5</li>
                  @endif
                @endif
              @endif
            @endif
          @endif

        </ul>
      </div>
    </div>
    @include('old_content.modals.claimitem')
    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="100">
    <?php 

    foreach($items as $item): 
      
      for($i=0; $i<count($unlocked); $i++){
        if($item->tier == $unlocked[$i]){ ?>
           
        <div class="col-lg-4 col-md-6 portfolio-item <?php echo $item->tier; ?>">
          <div class="portfolio-wrap">
            <img src="<?php echo $item->src; ?>" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a id="claim-item" data-toggle="modal" data-id="{{$item->id}}"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

    <?php } 
    
    }
    endforeach; ?>
     
    </div>

  </div>
</section><!-- End Portfolio Section -->

<script>
$(document).ready(function () {
/* Edit task */
$('body').on('click', '#claim-item', function () {
var item_id = $(this).data('id');
$.get('tieritems/'+item_id+'/edit', function (data) {
$('#claimitem').modal('show');
$('#itemimage').attr("src",data.src);
$('#item').val(data.src);
})
});
});
</script>