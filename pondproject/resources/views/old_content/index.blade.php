@extends('about')

@section('main')
      <div class="row">
        <div class="col">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Task activities.</p>
            <!--<a href="#" class="btn btn-primary">Complete</a>-->

            <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalLong">
              Complete <?php echo $i;?>
              </button>

            <a href="#" class="btn btn-info btn-sm">Edit</a>

            @foreach($tasks as $tasks)
            <div class="card">
              <div class="card-body">
                <div class="card-title">
                  <p>{{$tasks->title}}</p>
                </div>

                <div class="card-text">
                  <p>{{$tasks->detail}}</p>
                  <p>{{$tasks->deadline}}</p>
                </div>
              </div>
            </div>
            @endforeach

          </div>
        </div>
      </div>
@endsection
