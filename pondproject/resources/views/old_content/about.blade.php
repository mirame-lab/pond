<?php 
if(($taskcompnum != 0)&&(count($task) != 0)){
  $prog = round((($taskcompnum/count($task))*100));
}
else{
  $prog = 0;
}
 ?>
<!-- ======= Tasks Section ======= -->
<section id="tasks" class="about skills">
  <div class="container">

    <div class="section-title">
      <h2>TASKS</h2>
    </div>

<!-- Modals -->  
@include('old_content.modals.add_modal')
@include('old_content.modals.edit_modal')


<?php if(count($task) != 0){?>
  <div class="row skills-content">
<div class="col-lg-12" data-aos="fade-up">

  <div class="progress" >
    <span class="skill">Your Progress: <i class="val">
    <?php 
      
      echo $prog.'%';
      
      ?></i></span>
    <div class="progress-bar-wrap" >
      <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $prog; ?>" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>
      
</div>

<?php } 
    else{?>
      <div class="section-title" style="margin: 5%; text-align: center;">
      <p>&nbsp&nbsp&nbspNo tasks!</p>
      </div>
      <?php   }?>


</div>
         
   
    <!-- Button to Open the Modal -->
      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalLoginForm">
        Add task
      </button>
      <br> </br>

    <!-- Modal HTML Markup -->
    <div class="grid-container"> 
      <?php
      // $modals = array( 'a', 'b', 'c', 'd', 'e' );// Set the array
      
      foreach( $task as $tasks ):
      ?>

      @include('old_content.modals.tierunlocked')
   
      
      

        <div class="card" style="max-width: 300px;min-width: 300px; height:300px; ">
      
      <div class="card-body" >
        <h5 class="card-title">{{$tasks->title}}</h5>
        <p class="round3" ></p>
        <p class="card-text">{{$tasks->detail}}</p>
        <p class="card-text" id="deadline">{{$tasks->deadline}}</p>
        <input class="test" type="hidden" value="{{$tasks->deadline}}"></input>
        <input class="task_status" type="hidden" value="{{$tasks->isCompleted}}"></input>

        <form action="{{route('tasks.update',$tasks->id)}}" method="post" >
          @method('patch')
          @csrf
            <button type="submit" class="btn btn-primary btn-sm presscompleted" data-toggle="modal" data-target="#exampleModalLong"
              onClick="{{TaskController::addPoints()}} check()">
              Complete 
            </button> 
        </form>



        <a class="btn btn-primary btn-sm" id="edit-task" data-toggle="modal" data-id="{{$tasks->id}}">
         Edit
        </a>

        <form action="{{ route('tasks.destroy', $tasks->id ) }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" href="" class="btn md-5 btn-danger btn-sm">Delete</button>
                @csrf
        </form>

          </div>
  </div>   
       
        
        
   

    <?php
  
      endforeach;
    ?>
  </div>
  </div>
</section>


<script>

 var test = document.querySelectorAll("p.round3"),i;
 var dline= document.querySelectorAll("input.test");
 var tid = document.querySelectorAll("input.task_status");
 var completed = document.querySelectorAll(".presscompleted"),j;
 var today = new Date();
 var deadline;
 
 if(test.length){
  
  for( i=0; i<test.length; i++){
     
     deadline = new Date(dline[i].value);
    //  alert(deadline + " > " + today + " is "+(deadline>today));
    if(today>deadline){
      test[i].style.backgroundColor= "red";
      test[i].innerHTML = "overdue";
      <?php TaskController::delPoints();?>
    }
    else{
      test[i].style.backgroundColor= "green";
      test[i].innerHTML = "ongoing";
    }

  }
 }
 
 for(j=0; j<completed.length; j++){
  //  alert("task #"+index+" completed!");
   if(tid[j].value == 1){
    completed[j].disabled = true;
    completed[j].style.backgroundColor = "#98a3b5";
    completed[j].style.borderColor = "#656970";
    test[j].style.backgroundColor= "#3c81fa";
    test[j].innerHTML = "completed";
   
   }
   
 }

 function check(){
    <?php TaskController::addLvlPoints($prog); ?>
 }

 $(document).ready(function () {
/* Edit task */
$('body').on('click', '#edit-task', function () {
var task_id = $(this).data('id');
$.get('tasks/'+task_id+'/edit', function (data) {
$('#edittask').modal('show');
$('#taskid').val(data.id);
$('#title').val(data.title);
$('#detail').val(data.detail);
$('#date').val(data.deadline);
})
});
});

</script>

