
          <!-- Modal HTML Markup -->
          <div id="edittask" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title">Edit task</h1>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="tasks">
                
                  
                    <input type="hidden" name="_token" value="">
                    <input type="hidden" name="owner" value="<?php echo auth()->user()->email; ?>">
                    <input type="hidden" id="taskid" name="id" >
                    <div class="form-group">
                        <label class="control-label">Title</label>
                        <div>
                            <input type="text" id="title" class="form-control input-lg" name="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Description</label>
                        <div>
                            <input type="text" id="detail" class="form-control input-lg" name="detail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start</label>
                        <div id="datetimepicker2" class="input-append date">
                          <input data-format="dd/MM/yyyy" type="date" name="start"></input>
                          <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Deadline</label>
                        <div id="datetimepicker2" class="input-append date">
                          <input data-format="dd/MM/yyyy" type="date" id="date" name="deadline"></input>
                          <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                          </span>
                        </div>
                    </div>

                    <div class="form-group">
                    @csrf
                        <div>
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->