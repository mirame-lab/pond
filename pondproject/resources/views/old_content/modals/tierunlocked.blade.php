        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">CONGRATULATIONS!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                    </div>
                  <div class="modal-body">

                   Your points : <?php echo $currPoints;?> <br>
                   <h3>{{$currTier}} has been unlocked!</h3>
                   <a>The completed task will be deleted.</a>
                  </div>

                    <div class="modal-footer">
                    <form action="{{ route('tasks.destroy', $tasks->id ) }}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit" href="" class="btn btn-danger">Delete Task</button>
                  
                    </form>
                    </div>
                  </div>
                </div>
        </div>