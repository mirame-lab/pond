<div id="ModalLoginForm" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title">New task</h1>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="tasks">
                    <input type="hidden" name="owner" value="<?php echo auth()->user()->email; ?>">
                    <input type="hidden" name="_token" value="">
                    <div class="form-group">
                        <label class="control-label">Title</label>
                        <div>
                            <input type="text" class="form-control input-lg" name="title" value="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Description</label>
                        <div>
                            <input type="textarea" class="form-control input-lg" name="detail" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start</label>
                        <div id="datetimepicker2" class="input-append date">
                          <input data-format="dd/MM/yyyy" type="date" name="start" required></input>
                          <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Deadline</label>
                        <div id="datetimepicker2" class="input-append date">
                          <input data-format="dd/MM/yyyy" type="date" name="deadline" required></input>
                          <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div> 
                            <button type="submit" class="btn btn-success" name="Save" value="save">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->