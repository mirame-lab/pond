        <!-- Modal -->
<div class="modal fade" id="claimitem">
        <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" >Claim this item?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                    </div>
                  <div class="modal-body">
                  <div style=" text-align: center; margin: 10%">
                    <img src="" class="img-fluid" id="itemimage" alt="">
                  </div>
                 
                  <form role="form" id="claimform" method="POST" action="reflection">
                
                  
                    <input type="hidden" name="_token" value="">
                    <input type="hidden" name="owner" value="<?php echo auth()->user()->email; ?>">
                    <input type="hidden" id="item" name="item">
                    <div class="form-group">
                        <label class="control-label">Reflection</label>
                        <div>
                          <textarea name="reflection" form="claimform">What did it take?</textarea> 
                        </div>
                    </div>
                    
                    <div class="form-group">
                    @csrf
                    <div>
                            <button type="submit" class="btn btn-success">Claim</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    @csrf
                </form>

                  </div>

                    <div class="modal-footer">

                    </div>
                  </div>
        </div>
</div>