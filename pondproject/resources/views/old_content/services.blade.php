<!-- ======= Shop Section ======= -->
<section id="shop" class="services">
  <div class="container">

    <div class="section-title">
      <h2>HALL OF FAME</h2>
      <p> Welcome to the Hall of Fame where you can publish your decorated pond and achieve badges based on your views and likes!</p>
    </div>

    <div class="row">
      <div class="col-md">
        <div class="card ">
          <div class="card-header">
            <h4 class="card-title">Hall Of Fame</h4>
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
              <tr>
                <th class="text">
                  Players
                </th>
                <th class="text-center">
                  Scores
                </th>
              </tr>
                </thead>
                <tbody>
                
                <?php for($i=0; $i<sizeof($hof['names']); $i++){ ?>
                <tr>
                <td class="text">
                <!-- user names -->
                <?php echo $hof['names'][$i]['alias']; ?> 
                &nbsp &nbsp &nbsp &nbsp
                </td>
                <td class="text-center">
                <!-- user scores -->
                <?php echo $hof['scores'][$i]; ?> 
                </td>
                <tr>
                <!-- <br> -->
                <?php } ?>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- 
    <div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal"> 
    
      <div class="col-12 col-sm-6 col-lg-3">
        <img class="w-100" src="assets/img/HOF/pond-1.png" data-target="#carouselExample" data-slide-to="0">
      </div>
  
      <div class="col-12 col-sm-6 col-lg-3">
        <img class="w-100" src="assets/img/HOF/pond-2.png" data-target="#carouselExample" data-slide-to="1">
      </div>
    
      <div class="col-12 col-sm-6 col-lg-3">
        <img class="w-100" src="assets/img/HOF/pond-3.png" data-target="#carouselExample" data-slide-to="2">
      </div>
  
      <div class="col-12 col-sm-6 col-lg-3">
        <img class="w-100" src="assets/img/HOF/pond-4.jpg" data-target="#carouselExample" data-slide-to="3">
      </div>

      <div class="col-12 col-sm-6 col-lg-3">
        <img class="w-100" src="assets/img/HOF/pond-5.png" data-target="#carouselExample" data-slide-to="4">
      </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      Carousel markup: https://getbootstrap.com/docs/4.4/components/carousel/
      <div id="carouselExample" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="assets/img/HOF/pond-1.png">
            </div>

            <div class="carousel-item">
              <img class="d-block w-100" src="assets/img/HOF/pond-2.png">
            </div>

            <div class="carousel-item">
              <img class="d-block w-100" src="assets/img/HOF/pond-3.png">
            </div>

            <div class="carousel-item">
              <img class="d-block w-100" src="assets/img/HOF/pond-4.png">
            </div>

            <div class="carousel-item">
              <img class="d-block w-100" src="assets/img/HOF/pond-5.png">
            </div>

          </div>
          <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->
    <!--<div class="row">
      <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up">
        <div class="icon"><i class="icofont-computer"></i></div>
        <h4 class="title"><a href="">Lorem Ipsum</a></h4>
        <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
      </div>
      <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
        <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
        <h4 class="title"><a href="">Dolor Sitema</a></h4>
        <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
      </div>
      <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
        <div class="icon"><i class="icofont-earth"></i></div>
        <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
        <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
      </div>
      <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
        <div class="icon"><i class="icofont-image"></i></div>
        <h4 class="title"><a href="">Magni Dolores</a></h4>
        <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
      </div>
      <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
        <div class="icon"><i class="icofont-settings"></i></div>
        <h4 class="title"><a href="">Nemo Enim</a></h4>
        <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
      </div>
      <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
        <div class="icon"><i class="icofont-tasks-alt"></i></div>
        <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
        <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
      </div>
    </div> -->

  </div>
</section><!-- End hall of fame Section -->
